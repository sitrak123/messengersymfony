<?php
/**
 * @author <Akartis>
 * (c) akartis-dev <sitrakaleon23@gmail.com>
 * Do it with love
 */

namespace App\Service;


use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class MailNotification
{
	private MailerInterface $mailer;

	public function __construct(MailerInterface $mailer)
	{
		$this->mailer = $mailer;
	}

	public function sendMail(string $email)
	{
		$email = (new Email())
			->from('noresponse@sitraka.fr')
			->to($email)
			->subject('Mail')
			->html("<h1>Message test messenger</h1>");
		sleep(5);

		$this->mailer->send($email);
	}

}
