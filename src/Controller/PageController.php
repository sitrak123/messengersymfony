<?php
/**
 * @author <Akartis>
 * (c) akartis-dev <sitrakaleon23@gmail.com>
 * Do it with love
 */

namespace App\Controller;

use App\Form\MailType;
use App\Message\MailMessage;
use App\Service\MailNotification;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PageController extends AbstractController
{

	/**
	 * @Route(name="/")
	 * @param Request $request
	 * @param MailNotification $mail
	 * @return Response
	 */
	public function index(Request $request, MailNotification $mail):Response
	{
		$form = $this->createForm(MailType::class);
		$form->handleRequest($request);

		if($form->isSubmitted() && $form->isValid()){
			$email = $form->get('email')->getData();
			$this->dispatchMessage(new MailMessage($email));
			$this->addFlash('success', "Mail bien envoyer");
		}

		return $this->render("index.html.twig", [
			'form' => $form->createView()
		]);
	}
}
